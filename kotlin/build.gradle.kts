plugins {
    kotlin("jvm") version "1.3.71"
    id("me.champeau.gradle.jmh") version("0.5.0")
}

group = "info.kschamer"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://plugins.gradle.org/m2/")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    implementation("org.openjdk.jmh:jmh-core:1.23")
    implementation("org.openjdk.jmh:jmh-generator-annprocess:1.23")
    implementation("me.champeau.gradle:jmh-gradle-plugin:0.5.0")


    testImplementation("org.junit.jupiter:junit-jupiter:5.6.1")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }

    test {
        useJUnitPlatform()
    }
}