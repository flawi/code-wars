package info.kschamer.codewars.katas

import kotlin.math.sqrt

// see https://www.codewars.com/kata/backwards-read-primes/
fun backwardsPrime(start: Long, end: Long): String {
    return (start..end).filter { num: Long ->
        val reversed = reverseNumber(num)
        return@filter num != reversed && isPrime(num) && isPrime(reversed)
    }.joinToString(" ")
}

private fun isPrime(num: Long): Boolean {
    // sqrt(num) significantly faster than n/2!
    (2..sqrt(num.toDouble()).toLong()).forEach {
        if (num % it == 0L)
            return false
    }
    return true
}

private fun reverseNumber(num: Long): Long {
    var reversed = 0L
    var numToRun = num

    while (numToRun != 0L) {
        val digit = numToRun % 10
        reversed = reversed * 10 + digit
        numToRun /= 10
    }
    return reversed
}

fun main(args: Array<String>) {
    print(backwardsPrime(0, 100000000))
}


