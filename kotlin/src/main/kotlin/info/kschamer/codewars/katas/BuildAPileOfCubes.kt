package info.kschamer.codewars.katas

import kotlin.math.sqrt

object ASum {

    // https://www.codewars.com/kata/5592e3bd57b64d00f3000047
    fun findNb(m: Long): Long {
        var total = 0L
        var n = 0L
        while(m > total) {
            n++
            total += n * n * n
        }

        return if(m == total){
           n
        } else -1L
    }
}