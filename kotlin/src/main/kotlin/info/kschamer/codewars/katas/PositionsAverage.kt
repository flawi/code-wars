package info.kschamer.codewars.katas

object PositionAverage {

    /**
     * Holder of a combination of two sub-strings. Pair<> cannot be used since the combination of two identical
     * sub-strings would result in an equal Pair<> instance, which would replace each other in Set-like structures
     * (a Map is used to store the matched positions per combination).
     */
    class Combination<out A, out B>(private val left: A, private val right: B) {
        operator fun component1(): A = left
        operator fun component2(): B = right
    }

    fun posAverage(s: String): Double {
        val countMatches: (Combination<String, String>) -> Int = { (a: String, b: String) ->
            a.asIterable().withIndex().sumBy { (index, char) ->
                if (char == b[index]) 1 else 0
            }
        }
        return s.split(",")
            .map { it.trim() }
            .let { strings: List<String> ->
                // 1. step compute of possible combinations of sub-strings:
                //   -> iterating over all sub-string but the last
                strings.subList(0, strings.size - 1)
                    .withIndex()
                    .flatMap { (index, s) ->
                        // -> combine the current element with all following elements
                        strings.subList(index + 1, strings.size)
                            .map { subListString -> Combination(s, subListString) }
                    }
                    // 2. for each combination calculate the matching positions and store them in a Pair<>
                    .map { combination: Combination<String, String> -> Pair(combination, countMatches(combination)) }
                    .toMap()
                    // 3. calculate the sore
                    .let { combinationToMatch: Map<Combination<String, String>, Int> ->
                        val maxCommonPositions = combinationToMatch.size * strings.first().length
                        val actualCommonPositions = combinationToMatch.values.sum()
                        (actualCommonPositions.toDouble() / maxCommonPositions.toDouble()) * 100
                    }
            }
    }
}