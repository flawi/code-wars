package info.kschamer.codewars.katas

object ComplementaryDna {
    // https://www.codewars.com/kata/complementary-dna
    fun makeComplement(dna: String): String = dna.map {
        when (it) {
            'A' -> 'T'
            'T' -> 'A'
            'C' -> 'G'
            'G' -> 'C'
            else -> error("Cannot complement $it")
        }
    }.joinToString(separator = "")
}