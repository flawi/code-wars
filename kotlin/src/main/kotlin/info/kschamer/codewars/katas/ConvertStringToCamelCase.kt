package info.kschamer.codewars.katas


object ConvertStringToCamelCase {
    private val pattern = Regex("[A-Za-z]+")

    // https://www.codewars.com/kata/517abf86da9663f1d2000003/train/kotlin
    fun toCamelCase(str:String):String = pattern.findAll(str)
         .map { it.value }
         .mapIndexed { index, s -> if(index == 0) s else s.capitalize() }
         .joinToString("")
}