package info.kschamer.codewars.katas

import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class PositionAverageTest {
    fun assertFuzzy(s: String, exp: Double) {
        val inrange: Boolean
        val merr = 1e-9
        val actual = PositionAverage.posAverage(s)
        inrange = Math.abs(actual - exp) <= merr
        if (!inrange) {
            println("Expected mean must be near $exp, got $actual")
        }
        assertTrue(inrange)
    }


    @Test
    fun `test from task explanation`() {
        assertFuzzy("6900690040, 4690606946, 9990494604", 26.6666666667)
    }

    @Test
    fun `advanced test from task explanation`() {
        assertFuzzy("444996, 699990, 666690, 096904, 600644, 640646, 606469, 409694, 666094, 606490", 29.2592592593)
    }

    @Test
    fun `test 1`() {
        assertFuzzy("466960, 069060, 494940, 060069, 060090, 640009, 496464, 606900, 004000, 944096", 26.6666666667)
    }

    @Test
    fun `test 2`() {
        assertFuzzy("444996, 699990, 666690, 096904, 600644, 640646, 606469, 409694, 666094, 606490", 29.2592592593)
    }

    @Test
    fun `test 3`() {
        assertFuzzy("449404, 099090, 600999, 694460, 996066, 906406, 644994, 699094, 064990, 696046", 24.4444444444)
    }


    @Test
    fun `6900690040, 4690606946 should be 30,0`() {
        assertFuzzy("6900690040, 4690606946", 30.0)
    }

    @Test
    fun `6900690040, 6900690040 should be 100,0`() {
        assertFuzzy("6900690040, 6900690040", 100.0)
    }

    @Test
    fun `6900690040, 4694666996 should be 0,0`() {
        assertFuzzy("6900690040, 4694966996", 0.0)
    }

    @Test
    fun `99, 46, 06 should be 33,333333`() {
        assertFuzzy("99, 96, 06", 33.33333333333333)
    }

    @Test
    fun `0, 0, 1 should be 33,333333`() {
        assertFuzzy("0, 0, 1", 33.33333333333333)
    }
}
