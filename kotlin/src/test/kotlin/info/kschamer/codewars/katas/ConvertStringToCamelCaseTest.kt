package info.kschamer.codewars.katas

import info.kschamer.codewars.katas.ConvertStringToCamelCase.toCamelCase
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class ConvertStringToCamelCaseTest {
    @Test
    fun testFixed() {
        assertEquals("", toCamelCase(""))
        assertEquals("theStealthWarrior", toCamelCase("the_stealth_warrior"))
        assertEquals("TheStealthWarrior", toCamelCase("The-Stealth-Warrior"))
        assertEquals("ABC", toCamelCase("A-B-C"))
    }
}